package attestation01;

import java.util.Scanner;
import java.nio.file.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathFileWords {
    public static Path readPath() {
        System.out.print("Введите путь к файлу(words.txt): ");
        Scanner scanner = new Scanner(System.in);
        Path sourcePath = Paths.get(scanner.next());
        scanner.close();
        if (!Files.isReadable(sourcePath)) {
            throw new RuntimeException("File \"" + sourcePath.getFileName() + "\" does not found");
        }
        return sourcePath;
    }
}


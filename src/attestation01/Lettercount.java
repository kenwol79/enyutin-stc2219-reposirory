package attestation01;

import java.util.*;
import java.nio.file.*;
import java.util.stream.Stream;
import java.io.IOException;

public class Lettercount {
    public static void countChars(Path sourcePath) throws IOException {
        HashMap<Character, Integer> charMap = new HashMap<>();
        try (Stream<String> sourceStringStream = Files.lines(sourcePath)) {
            sourceStringStream
                    .map(sourceString -> sourceString.split("\\PL+"))
                    .flatMap(Arrays::stream)
                    .map(String::toLowerCase)
                    .flatMap(string -> string.chars().mapToObj(integer -> (char) integer))
                    .forEach(key -> charMap.merge(key, 1, Integer::sum));
        }
        WriteFile.writeFile(charMap, Paths.get("outputChars.txt"));
    }
}

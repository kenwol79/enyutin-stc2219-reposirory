package attestation01;

import java.io.IOException;
import java.nio.file.Path;
import java.lang.RuntimeException;


public class Main {
    public static void main(String[] args) {
        Path sourcePath = PathFileWords.readPath();
        try {
            Wordcount.countWords(sourcePath);
            Lettercount.countChars(sourcePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

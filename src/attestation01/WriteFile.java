package attestation01;

import java.util.*;
import java.nio.file.*;
import java.io.*;

public class WriteFile {
    public static <T> void writeFile(HashMap<T, Integer> counterMap, Path outputPath) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath.toFile()))) {
            counterMap.remove("");
            for (T key : new TreeSet<>(counterMap.keySet())) {
                writer.write(key + " - " + counterMap.get(key) + "\n");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

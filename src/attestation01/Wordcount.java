package attestation01;

import java.util.*;
import java.nio.file.*;
import java.util.stream.Stream;
import java.io.IOException;

public class Wordcount {
    public static void countWords(Path sourcePath) throws IOException {
        HashMap<String, Integer> wordMap = new HashMap<>();
        try (Stream<String> sourceStringStream = Files.lines(sourcePath)) {
            sourceStringStream
                    .map(sourceString -> sourceString.split("\\PL+"))
                    .flatMap(Arrays::stream)
                    .map(String::toLowerCase)
                    .forEach(key -> wordMap.merge(key, 1, Integer::sum));
        }
        WriteFile.writeFile(wordMap, Paths.get("outputWords.txt"));
    }

}

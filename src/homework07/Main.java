package homework07;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        UsersRepositoryFileImpl usersRepository = new UsersRepositoryFileImpl();

        System.out.println("Пользователь ID=1: " + usersRepository.findById(1));
        System.out.println("Пользователь ID=2: " + usersRepository.findById(2));
        System.out.println("Пользователь ID=3: " + usersRepository.findById(3));
        System.out.println("Пользователь ID=4: " + usersRepository.findById(4));
        System.out.println();

        usersRepository.create(new User(3, "Владимир", "Енютин", 43, false));
        printUsersBase("Пользователь ID=3 добавлен: ");
        usersRepository.create(new User(4,"Джамиль", "Рахимов", 30, true));
        printUsersBase("Пользователь ID=4 добавлен: ");
        System.out.println();

        User user = usersRepository.findById(1);
        user.setName("Владимир");
        user.setAge(27);
        usersRepository.update(user);
        printUsersBase("Пользователь ID=1 обновлен: ");
        user.setName("Иезекииль");
        user.setAge(33);
        usersRepository.update(user);
        printUsersBase("Пользователь ID=2 обновлен: ");
        System.out.println();

        usersRepository.delete(3);
        printUsersBase("Пользователь ID=3 удален: ");
        usersRepository.delete(4);
        printUsersBase("Пользователь ID=4 удален: ");

    }

    public static void printUsersBase(String message) {
        HashMap<Integer, User> usersBase = ReadUsers.readFromFile();
        System.out.print(message);
        System.out.println(usersBase.values());
    }
}

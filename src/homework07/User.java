package homework07;

import java.util.HashMap;

import static java.util.Collections.max;

public final class User {
    final int id;
    String name;
    String lastName;
    int age;
    boolean haveJob;

    public User(int id, String name, String lastName, int age, boolean haveJob) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.haveJob = haveJob;
    }

    public User(String name, String lastName, int age, boolean haveJob) {
        HashMap<Integer, User> usersBase = ReadUsers.readFromFile();
        if (usersBase.isEmpty()) {
            this.id = 1;
        } else {
            this.id = max(usersBase.keySet()) + 1;
        }
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.haveJob = haveJob;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHaveJob(boolean haveJob) {
        this.haveJob = haveJob;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + lastName + "|" + age + "|" + haveJob;
    }
}


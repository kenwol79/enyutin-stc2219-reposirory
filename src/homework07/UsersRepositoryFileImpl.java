package homework07;

import java.util.HashMap;

public class UsersRepositoryFileImpl implements ChangeUsers {
    @Override
    public User findById(int id) {
        HashMap<Integer, User> usersBase = ReadUsers.readFromFile();
        return usersBase.get(id);
    }

    @Override
    public void create(User user) throws IllegalArgumentException {
        HashMap<Integer, User> usersBase = ReadUsers.readFromFile();
        if (usersBase.containsKey(user.id) || user.id < 1) {
            throw new IllegalArgumentException("ID пользователя некорректный или уже используется");
        }
        usersBase.put(user.id, user);
        WriteUsers.writeToFile(usersBase);
    }

    @Override
    public void update(User user) throws IllegalArgumentException {
        HashMap<Integer, User> usersBase = ReadUsers.readFromFile();
        if (!usersBase.containsKey(user.id)) {
            throw new IllegalArgumentException("ID пользователя не существует");
        }
        usersBase.put(user.id, user);
        WriteUsers.writeToFile(usersBase);
    }

    @Override
    public void delete(int id) throws IllegalArgumentException {
        HashMap<Integer, User> usersBase = ReadUsers.readFromFile();
        if (!usersBase.containsKey(id)) {
            throw new IllegalArgumentException("ID пользователя не существует");
        }
        usersBase.remove(id);
        WriteUsers.writeToFile(usersBase);
    }
}

package homework07;

import java.io.BufferedReader;;
import java.io.FileReader;
import java.util.HashMap;
import java.io.IOException;

public class ReadUsers {
    public static HashMap<Integer, User> readFromFile() {
        BufferedReader reader;
        HashMap<Integer, User> usersBase = new HashMap<>();
        try {
            reader = new BufferedReader(new FileReader("Users.txt"));
            String userLine = reader.readLine();
            while (userLine != null) {
                String[] parsedUserLine = userLine.split("\\|");
                usersBase.put(Integer.parseInt(parsedUserLine[0]), new User(Integer.parseInt(parsedUserLine[0]),
                        parsedUserLine[1], parsedUserLine[2], Integer.parseInt(parsedUserLine[3]),
                        Boolean.parseBoolean(parsedUserLine[4])));
                userLine = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return usersBase;
    }
}

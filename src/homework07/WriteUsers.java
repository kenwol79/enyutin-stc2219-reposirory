package homework07;

import java.io.BufferedWriter;;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;

public class WriteUsers {
    public static void writeToFile(HashMap<Integer, User> userBase) {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter("Users.txt"));
            for (Map.Entry<Integer, User> entry : userBase.entrySet()) {
                User userLine = entry.getValue();
                writer.write(userLine.toString() + "\n");
            }
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
